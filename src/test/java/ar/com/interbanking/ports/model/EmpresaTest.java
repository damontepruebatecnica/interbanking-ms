package ar.com.interbanking.ports.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("local")
class EmpresaTest {

    @Test
    void testConstructorAndGetters() {
        String cuit = "123456789";
        String razonSocial = "Empresa Ejemplo";
        LocalDate fechaAdhesion = LocalDate.of(2023, 8, 10);

        Empresa empresa = new Empresa(cuit,razonSocial,fechaAdhesion);

        assertThat(empresa.getCuit()).isEqualTo(cuit);
        assertThat(empresa.getRazonSocial()).isEqualTo(razonSocial);
        assertThat(empresa.getFechaAdhesion()).isEqualTo(fechaAdhesion);
    }

    @Test
    void testSetters() {
        Empresa empresa = new Empresa();
        empresa.setCuit("123");
        empresa.setRazonSocial("Empresa A");
        empresa.setFechaAdhesion(LocalDate.of(2023, 8, 10));

        empresa.setId(1L);
        empresa.setCuit("456");
        empresa.setRazonSocial("Empresa B");
        empresa.setFechaAdhesion(LocalDate.of(2023, 8, 11));

        assertThat(empresa.getId()).isEqualTo(1L);
        assertThat(empresa.getCuit()).isEqualTo("456");
        assertThat(empresa.getRazonSocial()).isEqualTo("Empresa B");
        assertThat(empresa.getFechaAdhesion()).isEqualTo(LocalDate.of(2023, 8, 11));
    }

    @Test
    void testToString() {
        Empresa empresa = new Empresa();
        empresa.setCuit("123");
        empresa.setRazonSocial("Empresa A");
        empresa.setFechaAdhesion(LocalDate.of(2023, 8, 10));

        empresa.setId(1L);

        String expectedToString = "Empresa{" +
                "id=1" +
                ", cuit='123'" +
                ", razonSocial='Empresa A'" +
                ", fechaAdhesion=2023-08-10" +
                '}';

        assertThat(empresa.toString()).hasToString(expectedToString);
    }
}