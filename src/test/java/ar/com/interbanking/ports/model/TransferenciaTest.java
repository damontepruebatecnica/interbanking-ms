package ar.com.interbanking.ports.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("local")
class TransferenciaTest {

    @Test
    public void testGettersAndSetters() {
        Transferencia transferencia = new Transferencia();
        transferencia.setId(1L);
        transferencia.setImporte(BigDecimal.valueOf(100.0));
        transferencia.setCuentaDebito("123");
        transferencia.setCuentaCredito("456");
        transferencia.setFecha(LocalDate.of(2023, 8, 10));

        assertThat(transferencia.getId()).isEqualTo(1L);
        assertThat(transferencia.getImporte()).isEqualTo(BigDecimal.valueOf(100.0));
        assertThat(transferencia.getCuentaDebito()).isEqualTo("123");
        assertThat(transferencia.getCuentaCredito()).isEqualTo("456");
        assertThat(transferencia.getFecha()).isEqualTo(LocalDate.of(2023, 8, 10));
    }

    @Test
    public void testEmpresaGetterAndSetter() {
        Transferencia transferencia = new Transferencia();
        Empresa empresa = new Empresa("123456789", "Empresa Ejemplo", LocalDate.now());
        transferencia.setEmpresa(empresa);

        assertThat(transferencia.getEmpresa()).isEqualTo(empresa);
    }

    @Test
    public void testToString() {
        Transferencia transferencia = new Transferencia();
        transferencia.setId(1L);
        transferencia.setImporte(BigDecimal.valueOf(100.0));
        transferencia.setCuentaDebito("123");
        transferencia.setCuentaCredito("456");
        transferencia.setFecha(LocalDate.of(2023, 8, 10));

        String expectedToString = "Transferencia{" +
                "id=1" +
                ", importe=100.0" +
                ", cuentaDebito='123'" +
                ", cuentaCredito='456'" +
                ", fecha=2023-08-10" +
                ", empresa=null" +
                "}";

        assertThat(transferencia.toString()).hasToString(expectedToString);
    }
}