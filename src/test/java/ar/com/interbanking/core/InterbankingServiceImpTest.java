package ar.com.interbanking.core;

import ar.com.interbanking.commons.data.EmpresaRequest;
import ar.com.interbanking.ports.model.Empresa;
import ar.com.interbanking.ports.repository.EmpresaRespository;
import ar.com.interbanking.ports.repository.TransferenciaRespository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("local")
@AutoConfigureMockMvc
class InterbankingServiceImpTest {

    @Mock
    EmpresaRespository respository;

    @InjectMocks
    InterbankingServiceImp servicio;

    @Test
    void obtenerEmpresasTransferenciasUltimoMes() throws Exception {

        List<Empresa> empresasMock = new ArrayList<>();
        LocalDate lastMonthStart = LocalDate.now().minusMonths(1).withDayOfMonth(1);
        LocalDate thisMonthStart = LocalDate.now().withDayOfMonth(1);
        when(respository.findEmpresasWithTransferenciasLastMonth(lastMonthStart, thisMonthStart)).thenReturn(empresasMock);

        List<Empresa> empresasResultantes = servicio.obtenerEmpresasTransferenciasUltimoMes();

        verify(respository, times(1)).findEmpresasWithTransferenciasLastMonth(lastMonthStart, thisMonthStart);
        assertEquals(empresasMock, empresasResultantes);
    }

    @Test
    void obtenerEmpresasAdheridasUltimoMes() {

        List<Empresa> empresasMock = new ArrayList<>();
        LocalDate lastMonthStart = LocalDate.now().minusMonths(1).withDayOfMonth(1);
        LocalDate thisMonthStart = LocalDate.now().withDayOfMonth(1);
        when(respository.findEmpresasFromLastMonth(lastMonthStart, thisMonthStart)).thenReturn(empresasMock);

        List<Empresa> empresasResultantes = servicio.obtenerEmpresasAdheridasUltimoMes();

        verify(respository, times(1)).findEmpresasFromLastMonth(lastMonthStart, thisMonthStart);
        assertEquals(empresasMock, empresasResultantes);
    }

    @Test
    void realizarAdhesionEmpresa() {

        EmpresaRequest empReq = new EmpresaRequest("1234557","prueba");

        when(respository.save(any())).thenReturn(new Empresa(empReq.getCuitEmpresa(),empReq.getRazonSocialEmpresa(),LocalDate.now()));

        servicio.realizarAdhesionEmpresa(empReq);

        verify(respository, times(1)).save(any());

    }
}