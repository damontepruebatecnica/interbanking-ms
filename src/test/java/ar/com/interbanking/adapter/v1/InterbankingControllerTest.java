package ar.com.interbanking.adapter.v1;

import ar.com.interbanking.commons.data.EmpresaRequest;
import ar.com.interbanking.commons.to.ErrorResponse;
import ar.com.interbanking.core.IInterbankingService;
import ar.com.interbanking.ports.model.Empresa;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("local")
class InterbankingControllerTest {

    @Autowired
    TestRestTemplate restTemplate;

    @InjectMocks
    InterbankingController controller;

    @Mock
    private IInterbankingService servicioMock;


    private ExceptionHandler exceptionHandler;

    @BeforeEach
    public void setup() {

        exceptionHandler = new ExceptionHandler();
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testExceptionHandler() {

        Exception mockException = new Exception("Test Exception");

        ResponseEntity<ErrorResponse> responseEntity = exceptionHandler.handler(mockException);

        assert responseEntity.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR;
        assert responseEntity.getBody() != null;
        assert responseEntity.getBody().getCode().equals("500");
        assert responseEntity.getBody().getMessage().equals(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }


    @Test
    void testGetEmpresasTranferenciaUltimoMes() {

        List<Empresa> empresasMock = new ArrayList<>();
        when(servicioMock.obtenerEmpresasTransferenciasUltimoMes()).thenReturn(empresasMock);

        ResponseEntity<List<Empresa>> response = controller.getEmpresasTranferenciaUltimoMes();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(empresasMock, response.getBody());
    }

    @Test
    void testGetEmpresasAdheridasUltimoMes() {

        List<Empresa> empresasMock = new ArrayList<>();
        when(servicioMock.obtenerEmpresasAdheridasUltimoMes()).thenReturn(empresasMock);

        ResponseEntity<List<Empresa>> response = controller.getEmpresasAdheridasUltimoMes();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(empresasMock, response.getBody());
    }

    @Test
    void adherirEmpresa() {

        EmpresaRequest emp = new EmpresaRequest("12345","Prueba");

        HttpHeaders headers = new HttpHeaders();
        headers.set("content-type", MediaType.APPLICATION_JSON.toString());
        headers.setBasicAuth("prueba","prueba123");
        HttpEntity<EmpresaRequest> request = new HttpEntity<>(emp, headers);

        ResponseEntity<EmpresaRequest> responseEntity = restTemplate.exchange("/v1/interbanking-ms/adherirEmpresa", HttpMethod.POST, request, EmpresaRequest.class);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }
}