package ar.com.interbanking.commons.data;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@SpringBootTest
@ActiveProfiles("local")
class EmpresaRequestTest {

    @Test
    void testConstructorAndGetters() {
        String cuit = "123456789";
        String razonSocial = "Empresa Ejemplo";
        LocalDate fechaAdhesion = LocalDate.of(2023, 8, 10);

        EmpresaRequest empresa = new EmpresaRequest(cuit, razonSocial);

        assertThat(empresa.getCuitEmpresa()).isEqualTo(cuit);
        assertThat(empresa.getRazonSocialEmpresa()).isEqualTo(razonSocial);
    }

    @Test
    void testSetters() {
        EmpresaRequest empresa = new EmpresaRequest("123", "Empresa A");

        empresa.setCuitEmpresa("456");
        empresa.setRazonSocialEmpresa("Empresa B");

        assertThat(empresa.getCuitEmpresa()).isEqualTo("456");
        assertThat(empresa.getRazonSocialEmpresa()).isEqualTo("Empresa B");
    }

    @Test
    void testEquals() {
        EmpresaRequest empresa1 = new EmpresaRequest("123", "Empresa A");
        EmpresaRequest empresa2 = new EmpresaRequest("123", "Empresa A");
        EmpresaRequest empresa3 = new EmpresaRequest("456", "Empresa B");

        assertAll("Comparing empresa1",
                () -> assertThat(empresa1).isEqualTo(empresa2),
                () -> assertThat(empresa1).isNotEqualTo(empresa3),
                () -> assertThat(empresa1).isNotEqualTo(null),
                () -> assertThat(empresa1).isNotEqualTo(new Object())
        );
    }

    @Test
    void testHashCode() {
        EmpresaRequest empresa1 = new EmpresaRequest("123", "Empresa A");
        EmpresaRequest empresa2 = new EmpresaRequest("123", "Empresa A");

        assertThat(empresa1.hashCode()).hasSameHashCodeAs(empresa2.hashCode());
    }

    @Test
    void testToString() {
        EmpresaRequest empresa = new EmpresaRequest("123", "Empresa A");

        String expectedToString = "EmpresaRequest{" +
                "cuitEmpresa='123'" +
                ", razonSocialEmpresa='Empresa A'" +
                '}';

        assertThat(empresa.toString()).hasToString(expectedToString);
    }

}