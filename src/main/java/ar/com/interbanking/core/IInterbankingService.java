package ar.com.interbanking.core;

import ar.com.interbanking.commons.data.EmpresaRequest;
import ar.com.interbanking.commons.data.TransferenciaRequest;
import ar.com.interbanking.ports.model.Empresa;

import java.util.List;

public interface IInterbankingService {

    List<Empresa> obtenerEmpresasTransferenciasUltimoMes();
    List<Empresa> obtenerEmpresasAdheridasUltimoMes();
    void realizarAdhesionEmpresa(EmpresaRequest request);

    void realizarTransferencia(TransferenciaRequest request);

}
