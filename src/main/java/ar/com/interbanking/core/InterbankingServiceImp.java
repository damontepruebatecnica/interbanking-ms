package ar.com.interbanking.core;

import ar.com.interbanking.commons.data.EmpresaRequest;
import ar.com.interbanking.commons.data.TransferenciaRequest;
import ar.com.interbanking.commons.exception.EmpresaException;
import ar.com.interbanking.ports.model.Empresa;
import ar.com.interbanking.ports.model.Transferencia;
import ar.com.interbanking.ports.repository.EmpresaRespository;
import ar.com.interbanking.ports.repository.TransferenciaRespository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class InterbankingServiceImp implements IInterbankingService{

    private final EmpresaRespository empRepo;
    private final TransferenciaRespository transferenciaRespository;

    public InterbankingServiceImp(EmpresaRespository empRepo, TransferenciaRespository transferenciaRespository){
        this.empRepo = empRepo;
        this.transferenciaRespository = transferenciaRespository;
    }


    @Override
    public List<Empresa> obtenerEmpresasTransferenciasUltimoMes() {
        LocalDate now = LocalDate.now();
        LocalDate lastMonthStart = now.minusMonths(1).withDayOfMonth(1);
        LocalDate thisMonthStart = now.withDayOfMonth(1);

        return empRepo.findEmpresasWithTransferenciasLastMonth(lastMonthStart, thisMonthStart);
    }

    @Override
    public List<Empresa> obtenerEmpresasAdheridasUltimoMes() {

        LocalDate now = LocalDate.now();
        LocalDate lastMonthStart = now.minusMonths(1).withDayOfMonth(1);
        LocalDate thisMonthStart = now.withDayOfMonth(1);

        return empRepo.findEmpresasFromLastMonth(lastMonthStart, thisMonthStart);
    }

    @Override
    public void realizarAdhesionEmpresa(EmpresaRequest empresa) {
        Empresa emp = new Empresa(empresa.getCuitEmpresa(),empresa.getRazonSocialEmpresa(),LocalDate.now());
        empRepo.save(emp);
    }

    @Override
    public void realizarTransferencia(TransferenciaRequest request) {
        Empresa empresa = empRepo.findByCuit(request.getCuitEmpresa());

        if (empresa == null){
            throw new EmpresaException("400","La empresa no existe");
        }
        Transferencia transferencia = new Transferencia();
        transferencia.setImporte(request.getImporte());
        transferencia.setCuentaDebito(request.getCuentaDebito());
        transferencia.setCuentaCredito(request.getCuentaCredito());
        transferencia.setFecha(LocalDate.now());
        transferencia.setEmpresa(empresa);

        transferenciaRespository.save(transferencia);
    }
}
