package ar.com.interbanking.ports.repository;

import ar.com.interbanking.ports.model.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface EmpresaRespository extends JpaRepository<Empresa, Long> {

    @Query("SELECT e FROM Empresa e " +
            "JOIN Transferencia t ON e.id = t.empresa.id " +
            "AND t.fecha >= :fechaInicio AND t.fecha < :fechaFin")
    List<Empresa> findEmpresasWithTransferenciasLastMonth(
            @Param("fechaInicio") LocalDate fechaInicio,
            @Param("fechaFin") LocalDate fechaFin);

    @Query("SELECT e FROM Empresa e WHERE e.fechaAdhesion >= :fechaInicio AND e.fechaAdhesion < :fechaFin")
    List<Empresa> findEmpresasFromLastMonth(@Param("fechaInicio") LocalDate fechaInicio, @Param("fechaFin") LocalDate fechaFin);


    Empresa findByCuit(String cuit);

}
