package ar.com.interbanking.ports.repository;

import ar.com.interbanking.ports.model.Transferencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransferenciaRespository extends JpaRepository<Transferencia, Long> {
}
