package ar.com.interbanking.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;



@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Value("${username.basic.auth}")
    private String usernameBasicAuth = "";

    @Value("${password.basic.auth}")
    private String passwordBasicAuth = "";

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(basicAuthInterceptor())
                .addPathPatterns("/v1/interbanking-ms/**");
    }

    @Bean
    public BasicAuthInterceptor basicAuthInterceptor() {
        return new BasicAuthInterceptor(usernameBasicAuth, passwordBasicAuth);
    }
}

