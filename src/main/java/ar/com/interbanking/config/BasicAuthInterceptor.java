package ar.com.interbanking.config;

import ar.com.interbanking.commons.constant.Constant;
import ar.com.interbanking.commons.exception.SecutiryException;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Base64;


public class BasicAuthInterceptor extends HandlerInterceptorAdapter {

    private String usernameBasicAuth = "";
    private String passwordBasicAuth = "";

    public BasicAuthInterceptor(String usernameBasicAuth, String passwordBasicAuth) {
        this.usernameBasicAuth = usernameBasicAuth;
        this.passwordBasicAuth = passwordBasicAuth;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String authToken = request.getHeader(Constant.AUTHORIZATION_HEADER);

        if (authToken != null) {
            if (authToken.contains(Constant.BASIC_HEADER_VALUE)) {
                if (!verifyBasicAuthentication(authToken)) {
                    throw new SecutiryException(Constant.UNAUTHORIZED_APP, "UNAUTHORIZED_APP", HttpStatus.UNAUTHORIZED);
                }
            } else {
                throw new SecutiryException(Constant.INVALID_TOKEN, "INVALID_TOKEN", HttpStatus.BAD_REQUEST);
            }
            return true;
        } else {
            throw new SecutiryException(Constant.INVALID_HEADER, "NULL_HEADER", HttpStatus.BAD_REQUEST);
        }
    }

    private boolean verifyBasicAuthentication(String basicToken) {
        String token = extractToken(basicToken);
        String credentials = usernameBasicAuth + ":" + passwordBasicAuth;

        return token.equals(getEncodeBase64(credentials.getBytes()));
    }

    private String extractToken(String authToken) {
        try {
            return authToken.split(" ")[1];
        } catch (Exception ex) {
            return "";
        }
    }

    private String getEncodeBase64(byte[] byteArray) {
        return Base64.getEncoder().encodeToString(byteArray);
    }
}
