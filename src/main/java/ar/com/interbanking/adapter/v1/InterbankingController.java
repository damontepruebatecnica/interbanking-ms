package ar.com.interbanking.adapter.v1;

import ar.com.interbanking.commons.data.EmpresaRequest;
import ar.com.interbanking.commons.data.TransferenciaRequest;
import ar.com.interbanking.core.IInterbankingService;
import ar.com.interbanking.ports.model.Empresa;
import ar.com.interbanking.commons.route.Route;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = Route.V1, produces = MediaType.APPLICATION_JSON_VALUE)
public class InterbankingController {

    private final IInterbankingService service;

    public InterbankingController(IInterbankingService service){
        this.service = service;
    }

    @GetMapping(Route.Interbanking.GET_EMPRESAS_TRANSFERENCIA_ULTIMO_MES)
    public ResponseEntity<List<Empresa>> getEmpresasTranferenciaUltimoMes()  {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(service.obtenerEmpresasTransferenciasUltimoMes());
    }

    @GetMapping(Route.Interbanking.GET_EMPRESAS_ADHERIDAS_ULTIMO_MES)
    public ResponseEntity<List<Empresa>> getEmpresasAdheridasUltimoMes(){
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(service.obtenerEmpresasAdheridasUltimoMes());
    }

    @PostMapping(Route.Interbanking.POST_ADHERIR_EMPRESA)
    public ResponseEntity<EmpresaRequest> adherirEmpresa(@RequestBody EmpresaRequest request){
        service.realizarAdhesionEmpresa(request);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(request);
    }


    @PostMapping(Route.Interbanking.POST_REALIZAR_TRANSFERENCIA)
    public ResponseEntity<TransferenciaRequest> transferencia(@RequestBody TransferenciaRequest request) {
        service.realizarTransferencia(request);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(request);
    }

}
