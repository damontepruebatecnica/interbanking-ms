package ar.com.interbanking.adapter.v1;

import ar.com.interbanking.commons.exception.EmpresaException;
import ar.com.interbanking.commons.exception.SecutiryException;
import ar.com.interbanking.commons.to.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handler(Exception e){
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ErrorResponse("500",HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()));
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(SecutiryException.class)
    public ResponseEntity<ErrorResponse> handlerSecutiryException(SecutiryException e){
        return ResponseEntity
                .status(e.getHttpStatus())
                .body(new ErrorResponse("401",e.getMessage()));
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(EmpresaException.class)
    public ResponseEntity<ErrorResponse> handlerSecutiryException(EmpresaException e){
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse(e.getCode(),e.getMessage()));
    }
}
