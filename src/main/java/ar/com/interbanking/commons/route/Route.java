package ar.com.interbanking.commons.route;

public class Route {

    public static final String V1 = "/v1";

    public static class Interbanking {

        public static final String GET_EMPRESAS_TRANSFERENCIA_ULTIMO_MES = "/interbanking-ms/empresasTransferenciasUltimoMes";
        public static final String GET_EMPRESAS_ADHERIDAS_ULTIMO_MES = "/interbanking-ms/empresasAdheridasUltimoMes";
        public static final String POST_ADHERIR_EMPRESA = "/interbanking-ms/adherirEmpresa";
        public static final String POST_REALIZAR_TRANSFERENCIA = "/interbanking-ms/realizarTransferencia";
    }
}
