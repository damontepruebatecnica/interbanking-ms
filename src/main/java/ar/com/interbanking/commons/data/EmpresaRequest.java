package ar.com.interbanking.commons.data;

import java.util.Objects;

public class EmpresaRequest {

    private String cuitEmpresa;
    private String razonSocialEmpresa;

    public EmpresaRequest(String cuitEmpresa, String razonSocialEmpresa) {
        this.cuitEmpresa = cuitEmpresa;
        this.razonSocialEmpresa = razonSocialEmpresa;
    }

    public String getCuitEmpresa() {
        return cuitEmpresa;
    }

    public void setCuitEmpresa(String cuitEmpresa) {
        this.cuitEmpresa = cuitEmpresa;
    }

    public String getRazonSocialEmpresa() {
        return razonSocialEmpresa;
    }

    public void setRazonSocialEmpresa(String razonSocialEmpresa) {
        this.razonSocialEmpresa = razonSocialEmpresa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmpresaRequest that = (EmpresaRequest) o;
        return Objects.equals(cuitEmpresa, that.cuitEmpresa) && Objects.equals(razonSocialEmpresa, that.razonSocialEmpresa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cuitEmpresa, razonSocialEmpresa);
    }

    @Override
    public String toString() {
        return "EmpresaRequest{" +
                "cuitEmpresa='" + cuitEmpresa + '\'' +
                ", razonSocialEmpresa='" + razonSocialEmpresa + '\'' +
                '}';
    }
}
