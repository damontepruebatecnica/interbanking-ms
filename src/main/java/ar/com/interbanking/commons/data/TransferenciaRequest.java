package ar.com.interbanking.commons.data;

import java.math.BigDecimal;

public class TransferenciaRequest {

    private BigDecimal importe;
    private String cuentaDebito;
    private String cuentaCredito;
    private String cuitEmpresa;

    public TransferenciaRequest(BigDecimal importe, String cuentaDebito, String cuentaCredito, String cuitEmpresa) {
        this.importe = importe;
        this.cuentaDebito = cuentaDebito;
        this.cuentaCredito = cuentaCredito;
        this.cuitEmpresa = cuitEmpresa;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getCuentaDebito() {
        return cuentaDebito;
    }

    public void setCuentaDebito(String cuentaDebito) {
        this.cuentaDebito = cuentaDebito;
    }

    public String getCuentaCredito() {
        return cuentaCredito;
    }

    public void setCuentaCredito(String cuentaCredito) {
        this.cuentaCredito = cuentaCredito;
    }

    public String getCuitEmpresa() {
        return cuitEmpresa;
    }

    public void setCuitEmpresa(String cuitEmpresa) {
        this.cuitEmpresa = cuitEmpresa;
    }

    @Override
    public String toString() {
        return "TransferenciaRequest{" +
                "importe='" + importe + '\'' +
                ", cuentaDebito='" + cuentaDebito + '\'' +
                ", cuentaCredito='" + cuentaCredito + '\'' +
                ", cuitEmpresa='" + cuitEmpresa + '\'' +
                '}';
    }
}
