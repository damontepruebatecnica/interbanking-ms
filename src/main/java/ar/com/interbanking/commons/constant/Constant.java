package ar.com.interbanking.commons.constant;

public class Constant {

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String BASIC_HEADER_VALUE = "Basic ";
    public static final String UNAUTHORIZED_APP = "action.unauthorized";
    public static final String INVALID_HEADER = "invalid.header";
    public static final String INVALID_TOKEN = "invalid.token";
}
