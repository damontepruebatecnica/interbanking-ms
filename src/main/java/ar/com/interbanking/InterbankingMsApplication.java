package ar.com.interbanking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterbankingMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterbankingMsApplication.class, args);
	}

}
