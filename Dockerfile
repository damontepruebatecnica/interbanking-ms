FROM maven:3.6.3-adoptopenjdk-11

COPY ./pom.xml ./project/pom.xml
COPY ./src/ ./project/src
WORKDIR ./project/
RUN mvn clean install

FROM openjdk:11-jdk-slim
COPY --from=0 ./project/target/interbanking-ms-0.0.1-SNAPSHOT.jar ./target/interbanking-ms-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["sh", "-c", "java -jar ./target/interbanking-ms-0.0.1-SNAPSHOT.jar"]